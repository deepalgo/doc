# Pipelines : Manage your documentation

## Overview
- [Pipelines Menu](#pipelines-menu)
- [Pipelines Warning](#pipelines-warning)
- [Pipelines Logs (for support purposes)](#pipelines-log-for-support-purposes)
- [Go Back to Deep Algo Docs Home](README.md)

## Pipelines Menu

This menu lets you manage and see the current documentation of your projects. From here you can manually launch a new version of your project or simply monitor the progress of a running documentation.
This menu will automatically be displayed after you create a new project.

![Pipelines Menu](img/pipeline-menu.png)

Deep algo documents only what you want to document.
You only need to add a tag `@deepalgo` in the comments just above the method or the variable you want to document.
So, we assume you have put at least one comment with text `@deepalgo` in your code.

Look at the [Guidelines to add comments](ci-cd/guidelines-add-comment.md)

Tips:
. If you flag a virtual method of an abstract class or an interface, all Use Cases of its implementation will be documented.
. All hand written comments above a method will also be included in the final documentation.

When you’re done tagging all your methods, you can click the Launch New version button in the upper right corner.
![New Version Button](img/new-version-button.png)

All the steps and logs of the technical analysis performed by Deep Algo is reported on this page:
- [Pipelines Warning](#pipelines-warning)
- [Pipelines Logs (for support purposes)](#pipelines-log-for-support-purposes)

When the pipeline is over (the status is green), you can start consulting the documentation:
- in the [docs menu](docs/docs.md) or,
- in your repository if you activated the toggle to push automatically the documentation when the pipeline succeeded.

In case of an error, both you and our support team will be notified to clear the issue.

The error message can be:
- no `@deepalgo` tag was found in the source code selected in the Git Source Field of your project's settings .
- a solving error from Deep Algo because our understanding rate is not enough to provide an effective documentation

Depending on the size of your project, the analysis can last between a couple of minutes and a couple of hours.

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Pipelines Warning

![Pipelines warnings](img/pipeline-menu-warning.png)

This report warns you about:
- if you didn't flag correctly your code with `@deepalgo`comments.
- List of methods with a lot of external calls and too few comments.
- List of methods with no test
- List of methods with a low test coverage.
- List of methods with possible performance issue

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Pipelines Logs (for support purposes)

![Pipelines Logs](img/pipeline-menu-log.png)

These are the logs of all the analysis. You can check where is the analysis
process and how long it lasts.

If everything is green, it's perfect !

If you have a red step, please send us the logs in Intercom chatbot ![Intercom](img/intercom_chatbot.png).
You can easily copy/paste the logs using this icon ![copy-paste-log](img/copy-paste-log.png).

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
