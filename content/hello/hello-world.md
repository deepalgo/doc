# Quick Start with our Hello World

The **Hello World** project is a time-honored tradition in computer programming.
It is a simple exercise that gets you started when learning something new.

Let's get started with Deep ALgo!

## Overview
You'll learn how to :
- Step 1: [Create a project](#step-1-create-a-project)
- Step 2: [Start an analysis](#step-2-start-an-analysis)
- Step 3: [Open the documentation](#step-3-open-the-documentation)
- [Go Back to Deep Algo Docs Home](README.md)


## Prerequisites

To complete this tutorial, you need a Deep Algo Account (https://app.deepalgo.com) and Internet access.
You don't need to know how to code, use the command line, or install applications on your computer.

> **TIP:** Open this guide in a separate browser window (or tab) so you can see it while you complete the steps in the tutorial.

## Step 1: Create a Project
By default, your project is created in your private organization which is named
with your firstname and your lastname.
Your private organization cannot be shared with other members.

1. Click on the `NEW PROJECT` button.
2. complete the form with the following data:
    - `Name`: My Hello World
    - `Organization`: private
    - `Git URL`: https://gitlab.com/deepalgolab/hello-world.git

![hello-world-new-project](img/hello-world-new-project.png)

3. Click on the `CREATE PROJECT` button

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Step 2: Start an analysis

When you create a project, it'll start automatically a first version of your
documentation.

In the `My Hello World` project, we already add '@deepalgo' comments in the code
to get a first documentation.
If you want to know more how to add `@deepalgo`comments, read the [Guidelines to add comments](ci-cd/guidelines-add-comment.md).

To start a new analysis:
1. open the `Hello World project`,
2. go to the `Pipelines` Menu

![hello-world-pipelines](img/hello-world-pipelines.png)

3. click on the new version button
![new-version-button](img/new-version-button.png)

Depending on how many lines of code you want to document, it can takes several
minutes.

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Step 3: Open the documentation

Stay on the `My Hello World` project page and go to `Docs`Menu.

![hello-world-docs](img/hello-world-docs.png)

You can consult the 3 types of documentation:
1. the `doc-as-code` Documentation

2. the `Interactiv docs`

![hello-world-interactiv-documentation](img/hello-world-interactiv-documentation.png)

3. the `doc on demand`.
It's the Interactiv type documentation generated on-demand by the user without
adding `@deepalgo`comment.

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Celebrate

By completing this tutorial, you’ve learned to create a project and make a new
documentation on Deep ALgo!

Here’s what you accomplished in this tutorial:

- Created a new project
- Started an analysis
- Opened the documentation

> **TIP:** To learn more about the power of Deep Algo, we recommend reading the [Deep Algo Docs Home](README.md).
