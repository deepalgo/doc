# Quick Access

[[_TOC_]]

# Deep Algo Docs

Welcome to [Deep Algo](https://www.deepalgo.com/) Documentation!

We’re glad to have you here!

As a Deep Algo user you’ll have access to all features your subscription includes.
Overview

# What is Deep Algo?

As a developer, incomplete or outdated documentation is a pervasive problem.

Deep Algo is SaaS platform which provides an automated and up-to-date documentation of your code base.

You eventually get a living insight of you business algorithms.

## Prerequisites
- Your source code is managed by a Git source control like Gitlab, Github or Bitbucket. We just need the Git URL and the read access credentials.

- Our **supported Web browsers**
Connect to Deep Algo from the web on your desktop anytime at https://app.deepalgo.com

| Browser   | Requirements         |
|:----------|----------------------|
| Chrome    | Version 79 or above  |
| Chromium  |                      |
| Firefox   |                      |

- Unsupported browsers
To focus on delivering the best possible experience in Deep Algo, it is necessary
to keep the list of supported browsers short.

When a browser is no longer supported, we stop fixing pesky bugs and issues.

- **Mobile browsers**
We're working hard to have the solution working on mobile and tablets, but as
far as now we do not support the use on mobile because the user experience is
not good. We'll keep you inform as soon as this feature is available.

- As of today, the managed language is Java.

We’re working hard to add new languages. Don’t hesitate to tell us which one would be the most valuable for you ! Use the Chatbot at the bottom right corner.

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Use cases
- document as a code (Docs as Code) in english
- get an overview of your application
- document automatically methods and variables from your Git repository
- integrate the documentation workflow in your DevOps toolchain
- keep this documentation updated by integrating this workflow in your CI
- get an interactive documentation to simulate outcomes depending on selected conditions
- get warnings when your code needs more tests or documentation directly from the developer.

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Overview
Following is the overall workflow:
- [Connect](connect/connect.md) the Deep Algo platform (https://app.deepalgo.com)
- [Create](projects/create.md) a project
- [Launch](pipelines/pipelines.md) a documentation
- [Automate your documentation](ci-cd/ci-cd.md)

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Quick Start with our Hello World!

Let's start the [Hello World](hello/hello-world.md) tutorial!

# User Documentation

## Connection to Deep Algo

Here, you can find all the details to [Connect](connect/connect.md) the Deep Algo platform (https://app.deepalgo.com)


<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Create a project

The best way to create your first project is to read this doc: "[Create a project](projects/create.md)".

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## List your projects by organizations

Let's learn how you can easily get a [list of your projects](projects/project-list.md) by organizations.

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## The project page

![Settings Menu](img/settings-menu.png)

The project page contains different menu to manage :
- [Settings](settings/settings.md) Menu
- [Docs](docs/docs.md) Menu ([Docs as Code](docs/docs.md#doc-as-code), [Interactive doc](docs/docs.md#interactive-doc), [Doc on demand](docs/docs.md#doc-on-demand))
- [Pipelines](pipelines/pipelines.md) Menu
- [CI/CD](ci-cd/ci-cd.md) Menu

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Manage your organizations

For more information, learn how to [manage your organization](organizations/organizations.md).

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Manage your profile

Learn how to [manage your profile](profile/profile.md).


<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

# Support and Help Center

We use Intercom Chat both to enable real-time communication with our support team and fix your issues as soon as possible.

![Intercom](img/intercom_chatbot.png)

Feel free to contact us by clicking the Chatbot at the right bottom of your browser.

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

# Subscription plan

To get all information about our subscription plan, feel free to visit our website (https://www.deepalgo.com).

# What's new?

#### 20.1.3
What an easy question! Everything is new!  Enjoy!

The last version is 20.1.3. You can see the version you use by clicking on the
information icon ![information-icon](img/information-icon.png).

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

# What's next?

What about the next development of Deep Algo?

Help us to develop the most valuable features.

![Intercom](img/intercom_chatbot.png)

Give us some feedbacks using the Chatbot at the bottom right corner.

Some ideas:
- C# language
- ….

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

# Frequently Asked Questions (FAQ)

## What is out of Deep algo's scope?

- All the external components of your code (because we can't analyse a code we don’t have :) )
It’s eventually great because what you need it’s to document the code implemented by your team. If you need to document an external native API, it’s the purpose of another project, you can document with Deep Algo!

- Licenses: There is great to deal with this pain on the market!

- Class Diagrams: we’re sure you can find that in your IDE.

- Your Code of conduct :-)

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Which languages can Deep Algo analyze?
Deep Algo is language agnostic: it's independent of any specific programming
language. We only need to implement the grammar of the language.

Deep Algo can currently manage applications in Java.

We're working hard to add new languages in the coming months.

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## How long is an analysis?
Deep Algo is able to analyze 250k lines of a java code in around 3 hours.

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## What's your confidence level in an analysis?
For each analysis, we get an understanding indicator with a detailed report of
all the files, lines of code Deep Algo didn't understand.

<div align="right">
  <a type="button" class="btn btn-default" href="#quick-access">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
