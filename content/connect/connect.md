# Connection to Deep Algo

## Overview

- [Sign up](#sign-up-to-deep-algo) to Deep Algo
- [Log in](#log-in-to-deep-algo) to Deep Algo
- My [first connection](#my-first-connection) to Deep Algo
- [Go Back to Deep Algo Docs Home](README.md)

## Sign up to Deep Algo
You can either:
- Access Deep Algo using your Google, Github or LinkedIn profiles

| ![Github](img/github_logo.png)   | ![Google](img/google_logo.png)  |![Linkedin](img/linkedin_logo.png)|
|:------------------------|:-----------------------------------------|----------------------------------|

-  Or, create your profile on the platform by clicking on “sign up” at the bottom of the first page and enter your user name, email and password.
Deep Algo uses Auth0 (https://auth0.com/) as authentication and authorization system.

![Sign up](img/sign-up_deepalgo.png)

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Log in to Deep Algo

- Use either your google, github, linkedin account

| ![Github](img/github_logo.png)   | ![Google](img/google_logo.png)  |![Linkedin](img/linkedin_logo.png)|
|:------------------------|:-----------------------------------------|----------------------------------|

- or your auth0 profile as choosen when you signed up.

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## My first Connection

At your first connection, you'll arrive at a welcome page.

![First connection](img/my-first-connection.png)

In a click, you can :
- click either the “Create your first project here” link or “NEW PROJECT” in the title bar to create your first project link.
This opens the [New project page](create/create.md).

- create an organization. For more information, learn how to [manage your organization](organizations/organizations.md).

- Upgrade your subscription plan to get more from Deep Algo.

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
