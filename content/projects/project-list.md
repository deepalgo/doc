# List all your projects

All your projects are managed by organization.
For more information, learn how to [manage your organization](organizations/organizations.md).

## Overview

- [Get the list of your projects](#get-the-list-of-your-projects) by organization.
- [The list of your projects](#the-list-of-your-projects)
- [Go Back to Deep Algo Docs Home](README.md)

# Get the list of your projects

There're 2 ways to get the list of your projects by organization

- The Home button

![Create page](img/home-button.png)

Whenever you click on the home button, you'll get the list of all the projects
in your active organization.

- Jump to another organizations

If you want to get the list of project from a specific organizations, you only
 need to jump to the targeted organization using the dedicated menu in
the menu bar

![organizations](img/jump-organizations.png)

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

# The list of your projects

From the list of projects, you can access in a click to the project you want

![Project list](img/project-list.png)

On this page, you can also note:
- Analysis in progress
![Analysis in progress](img/analysis-in-progress.png)
- Analysis which failed
![Analysis failed](img/analysis-failed.png)
- Analysis succeeded
![Analysis succeeded](img/analysis-succeded.png)
- Analysis with warnings
![Analysis warnings](img/analysis-warnings.png)

Click on the icon if you want to get more information !

You can also delete a project.
![Project delete](img/project-delete.png)


<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
