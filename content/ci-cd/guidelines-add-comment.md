# Guidelines to add `@deepalgo` comment

- **On method returning something**.

'void' methods are not "explainable" by themselves since they may affect many different objects or variables in the code. By tagging non-void method, the documentation will concern the returned value and the return values only, even if this method also impacts other variables.
The `@deepalgo` comment must be set in a comment block **preceding the method**.

Example in java: [Example On Return Method](code/exampleOnReturnMethod.java)
​
- On a **specific variable**. Yet if

The `@deepalgo` comment must be set in a comment block **preceding the variable**.
In the following example, the variable 'object' will be documented. That is to say all its states will be described.

Example in java: [Example On Variable](code/exampleOnVariable.java)
​
- **Using polymorphism**.

One powerful aspect of our technology comes with the overloading / overwriting management. To document all implementations of a methods defined in an interface, abstract class and simple class, only one `@deepalgo` comment has to be set on the method belonging highest class in the hierarchy.

The `@deepalgo` comment must be set in a comment block **preceding the method declaration**. In the following example, all specific implementations will be documented in a unique file.

Example in java: [Example On Interface](code/exampleOnInterface.java)


- **Anotations**.

When your method is preceeded with anotations (such as `@Override`) make sure to put the `@deepalgo` BEFORE the anotation
