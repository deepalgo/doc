# Automate your documentation

You can automate the documentation of your code by adding Deep Algo to your CI/CD.

## Overview
- [CI/CD Menu](#ci-cd-menu)
- [Prerequisites](#prerequisites)
- [Call the Deep Algo API](#call-the-deep-algo-api)
- [Go Back to Deep Algo Docs Home](README.md)

## CI CD menu

![CI CD menu](img/ci-cd-menu.png)

We only need to click on `GENERATE` to get your API token.

Don't forget to test it before using it!

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Prerequisites
- `@deepalgo` We assume you have put at least one comment with text `@deepalgo` in your code.

Look at the [Guidelines to add comments](ci-cd/guidelines-add-comment.md)

- Project id: `666b308e-f7f1-4e1f-b2af-28a8ac630fae`
- Api token Generate and copy your API token for CI/CD purposes

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Call the Deep Algo API
Deep Algo is a SaaS platform providing a RESTfull API. We have chosen the `JsonApi` standard for it's simplicity and readiness. Concreatly speaking, generating the documentation of your application requires the creation of a new `ProjectVersion` resource.

- `curlclient`
```curl

curl -X POST \
     -H "Content-Type: application/vnd.api+json" \
     -H "Mime-Type: application/vnd.api+json" \
     -H "Atuhorization: Api your api token ..." \
     --data {"data":{"type":"projectVersions","attributes":{"project-id":"666b308e-f7f1-4e1f-b2af-28a8ac630fae"}}} \
     https://app.deepalgo.com/api/v0/project-versions
```


- `javascript` with `axios` client

```javascript

 const api = Axios.create({baseURL: "https://app.deepalgo.com",
                           headers: { Authorization: 'Api ' + your api token ... }})
 const res = await this.axios.post('/api/v0/project-versions',{"project-id": "666b308e-f7f1-4e1f-b2af-28a8ac630fae"})
```

- ruby client ... coming soon
```ruby


```

Example in GitLab:

In your `.gitlab-ci.yml`, you can add the previous command

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
