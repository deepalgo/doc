# Docs menu

## Overview
Deep algo generates for you 3 different types of documentation:

- [Docs as Code](docs/docs.md#doc-as-code),
- [Interactive doc](docs/docs.md#interactive-doc),
- [Doc on demand](docs/docs.md#doc-on-demand)
- [Go Back to Deep Algo Docs Home](README.md)

## Doc as code

A specific file is created for each flagged method.

The documentation contains:
- the signature of the method or the name of the variable.
- the origin of the data structure
- the link to the code
- the link to the Interactive documentation on Deep Algo platform
- a short summary of the method
- a list of inputs involved in the algorithm.
- Output examples (the default example when all conditions are false and two
  other examples).
- all possible results
- API Usage : inferred from code usage.
- all the comments embedded in the code above the method or the variable.
test coverage of the method or variable



<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Interactive doc

The interactive documentation is a documentation enhanced with a formal
calculator.

>**Tip:** In all docs-as-code documentation, you have, below the title, a link
to the related interactive documentation.

![interactive-documentation](img/interactive-documentation.png)

By activating the conditions you want to consider `True`, and clicking on the
`update documentation` button ![update-documentation](img/update-documentation.png),
you get the updated result as written in the code.

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Doc on demand

It's the Interactive type documentation generated on-demand by the user without
adding `@deepalgo`comment.

In all your Doc-as-Code files, you may want to understand a non-flagged method
or variable.

It’s not a problem! We can do it!

Just click on the desired  variable/method. The calculation of the documentation
will be triggered and published in this menu.

When the calculation is done, the tag “In progress” will be deleted.

>**Tip:** If you want to automatically generate the documentation of these components, don’t forget to flag them in your code with a tag `@deepalgo` as explained in the Documentation Menu.


This feature will be implemented in a future version of Deep Algo.
Tell us if your think it should be a top priority!

Use the Intercom chatbot: ![Intercom](img/intercom_chatbot.png)

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
