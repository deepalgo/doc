package my.org.app;

class ExempleOnReturnMethod {
    /* Your comments here
     * @deepalgo
     *
     * @param input[double] some information
     * @return SomeClass some other information
     */
    SomeClass run(double input){
        return new SomeClass();
    }
}
