package my.org.app;

interface YourInterface {
    // @deepalgo
    SomeResult run();
}


class FirstImplem implements YourInterface {
    SomeResult run(){
    }
}
class SecondImplem implements YourInterface {
    SomeResult run(){
    }
}

class ThirdImplem extends FirstImplem {
    SomeResult run(){
    }
}
